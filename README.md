Flatboard [![Flatboard](https://img.shields.io/badge/Flatboard-1.0.5-blue.svg)](http://flatboard.free.fr) [![Join the chat at https://gitter.im/Fred89/flatboard](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/Fred89/flatboard?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)
===================

Fast and lightweight bulletin board, Json powered.
We're just getting started, so if you have any questions, comments, bugs to report or enhancements that should be implemented, please do feel free to post those here on GitHub.  
Online demo/support: http://flatboard.free.fr

## Getting Started
- [Installation](#installation)
- [License](#license)

## Installation
1. Download the latest version from [flatboard.free.fr](http://flatboard.free.fr/download.php?file=flatboard_latest.zip)
2. Extract the archive.
3. Upload `flatboard.zip` and `index.php` to your server/hosting.
4. Visit your domain http://domain.com/flatboard
5. Follow the Flatboard Unpack & the Installer to configure your website.

## License
The MIT License (MIT)  
  
Copyright (c) 2015-2018 Flatboard  
  
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
  
The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.  
  
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
